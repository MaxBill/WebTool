package com.geek;// 本题为考试单行多行输入输出规范示例，无需提交，不计分。

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        while (in.hasNextInt()) {//注意while处理多个case
            int a = in.nextInt();
            int b = in.nextInt();

            int[] arr = new int[b];
            for (int i = 0; i < b; i++) {
                arr[i] = in.nextInt();
            }

            // 计算
            int[] numbers = new int[a];
            for (int i = 0; i < arr.length; i++) {
                int value = arr[i] - 1;
                numbers[value] = numbers[value] + 1;
            }
            // 找到最小值
            int min = 1000;
            for (int i = 0; i < numbers.length; i++) {
                int value = numbers[i];
                if (value < min) {
                    min = value;
                }
            }
            System.out.println(min);
        }

    }
}