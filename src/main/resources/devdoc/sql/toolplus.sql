/*
Navicat MySQL Data Transfer
Source Host           : zuoshuai.vicp.net:10468
Source Database       : toolplus
Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001
Date: 2018-08-21 15:19:10
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for t_rtb_ftp
-- ----------------------------
DROP TABLE IF EXISTS `t_rtb_ftp`;
CREATE TABLE `t_rtb_ftp` (
  `FTP_ID` varchar(50) NOT NULL COMMENT '主键',
  `FTP_NAME` varchar(50) DEFAULT NULL COMMENT '备注名称',
  `FTP_USER` varchar(50) DEFAULT NULL COMMENT '账户名称',
  `FTP_PASS` varchar(50) DEFAULT NULL COMMENT '账户密码',
  `FTP_PKEY` varchar(50) DEFAULT NULL COMMENT '私有秘钥',
  `FTP_HOST` varchar(50) DEFAULT NULL COMMENT '主机地址',
  `FTP_PORT` varchar(20) DEFAULT NULL COMMENT '主机端口',
  `FTP_MODE` varchar(20) DEFAULT NULL COMMENT '支持模式:FTP支持两种模式：Standard (PORT方式，主动方式)，Passive (PASV，被动方式)',
  `FTP_TYPE` varchar(50) DEFAULT NULL COMMENT '访问协议:1:ftp,2:ftps:3:sftp',
  `CREATE_TIME` datetime DEFAULT NULL COMMENT '创建时间',
  `UPDATE_TIME` datetime DEFAULT NULL COMMENT '更新时间',
  `CREATE_USER` varchar(50) DEFAULT NULL COMMENT '创建人',
  `UPDATE_USER` varchar(50) DEFAULT NULL COMMENT '更新人',
  PRIMARY KEY (`FTP_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='FTP业务表';

-- ----------------------------
-- Table structure for t_sys_log
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_log`;
CREATE TABLE `t_sys_log` (
  `LOG_ID` varchar(50) NOT NULL COMMENT '日志主键',
  `LOG_USER` varchar(50) DEFAULT NULL COMMENT '日志用户',
  `LOG_NAME` varchar(50) DEFAULT NULL COMMENT '日志名称',
  `LOG_URLS` varchar(500) DEFAULT NULL COMMENT '日志地址',
  `LOG_PATH` varchar(500) DEFAULT NULL COMMENT '日志链接',
  `LOG_ADDR` varchar(50) DEFAULT NULL COMMENT '日志地址',
  `LOG_AREA` varchar(50) DEFAULT NULL COMMENT '日志区域',
  `LOG_TIME` varchar(50) DEFAULT NULL COMMENT '日志时间',
  `LOG_TYPE` varchar(10) DEFAULT NULL COMMENT '日志类型',
  `LOG_SYSTEM` varchar(50) DEFAULT NULL COMMENT '系统',
  `LOG_METHOD` varchar(50) DEFAULT NULL COMMENT '请求方式',
  `LOG_BROWSER` varchar(50) DEFAULT NULL COMMENT '浏览器',
  `LOG_REQ_PARAM` varchar(10000) DEFAULT NULL COMMENT '请求参数',
  `LOG_RES_PARAM` longtext COMMENT '响应参数',
  PRIMARY KEY (`LOG_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统日志表';

-- ----------------------------
-- Table structure for t_sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_menu`;
CREATE TABLE `t_sys_menu` (
  `MENU_ID` varchar(50) NOT NULL COMMENT '菜单主键',
  `MENU_NAME` varchar(50) DEFAULT NULL COMMENT '菜单名称',
  `MENU_CODE` varchar(50) DEFAULT NULL COMMENT '菜单编码',
  `MENU_PATH` varchar(50) DEFAULT NULL COMMENT '菜单链接',
  `MENU_ICON` varchar(20) DEFAULT NULL COMMENT '菜单图标',
  `MENU_PID` varchar(50) DEFAULT NULL COMMENT '父级菜单',
  `MENU_SHOW` varchar(1) DEFAULT NULL COMMENT '是否显示，n：不显示，y：显示',
  `CREATE_TIME` datetime DEFAULT NULL COMMENT '创建时间',
  `UPDATE_TIME` datetime DEFAULT NULL COMMENT '更新时间',
  `CREATE_USER` varchar(50) DEFAULT NULL COMMENT '创建人',
  `UPDATE_USER` varchar(50) DEFAULT NULL COMMENT '更新人',
  PRIMARY KEY (`MENU_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统菜单表';

-- ----------------------------
-- Table structure for t_sys_role
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_role`;
CREATE TABLE `t_sys_role` (
  `ROLE_ID` varchar(50) NOT NULL COMMENT '角色主键',
  `ROLE_NAME` varchar(50) DEFAULT NULL COMMENT '角色名称',
  `ROLE_TYPE` varchar(1) DEFAULT NULL COMMENT '是否启用，0：不启用，1：启用',
  `CREATE_TIME` datetime DEFAULT NULL COMMENT '创建时间',
  `UPDATE_TIME` datetime DEFAULT NULL COMMENT '更新时间',
  `CREATE_USER` varchar(50) DEFAULT NULL COMMENT '创建人',
  `UPDATE_USER` varchar(50) DEFAULT NULL COMMENT '更新人',
  PRIMARY KEY (`ROLE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统角色表';

-- ----------------------------
-- Table structure for t_sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_role_menu`;
CREATE TABLE `t_sys_role_menu` (
  `RM_ID` varchar(50) NOT NULL COMMENT '角色菜单主键',
  `ROLE_ID` varchar(50) NOT NULL COMMENT '角色主键',
  `MENU_ID` varchar(50) NOT NULL COMMENT '菜单主键',
  `CREATE_TIME` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`RM_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统角色菜单表';

-- ----------------------------
-- Table structure for t_sys_user
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_user`;
CREATE TABLE `t_sys_user` (
  `USER_ID` varchar(50) NOT NULL COMMENT '用户主键',
  `USER_NAME` varchar(50) DEFAULT NULL COMMENT '用户名称',
  `USER_PASS` varchar(100) DEFAULT NULL COMMENT '用户密码',
  `USER_SALT` varchar(50) DEFAULT NULL COMMENT '密码加盐',
  `USER_MAIL` varchar(50) DEFAULT NULL COMMENT '用户邮箱',
  `USER_HEAD` varchar(50) DEFAULT NULL COMMENT '用户头像',
  `USER_TYPE` varchar(1) DEFAULT NULL COMMENT '用户类型，0：系统用户，1:普通用户',
  `USER_STATUS` varchar(1) DEFAULT NULL COMMENT '用户状态，y：启用，n:冻结',
  `CREATE_TIME` datetime DEFAULT NULL COMMENT '创建时间',
  `UPDATE_TIME` datetime DEFAULT NULL COMMENT '更新时间',
  `CREATE_USER` varchar(50) DEFAULT NULL COMMENT '创建人',
  `UPDATE_USER` varchar(50) DEFAULT NULL COMMENT '更新人',
  PRIMARY KEY (`USER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统用户表';

-- ----------------------------
-- Table structure for t_sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_user_role`;
CREATE TABLE `t_sys_user_role` (
  `UR_ID` varchar(50) NOT NULL COMMENT '用户角色主键',
  `ROLE_ID` varchar(50) NOT NULL COMMENT '角色主键',
  `USER_ID` varchar(50) NOT NULL COMMENT '用户主键',
  `CREATE_TIME` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`UR_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统用户角色表';
