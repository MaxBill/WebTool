/*
Navicat MySQL Data Transfer
Source Server Version : 50505
Source Database       : toolplus
Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001
Date: 2018-08-21 15:28:45
*/


-- ----------------------------
-- Records of t_sys_menu
-- ----------------------------
INSERT INTO `t_sys_menu` VALUES ('19391d0a6035479fb4690f195e00faa5', '数据监控', 'druidLog', '/druids/index', '&#xe60a;', '8d458deec37c4a848a3a38982bc59f88', 'y', '2018-07-30 13:11:49', '2018-08-03 16:55:22', 'root', null);
INSERT INTO `t_sys_menu` VALUES ('37665f54c51e4d1ca0f9afd3a2442c94', '文件传输', 'ftpMenu', '/rtb/ftp/list', '&#xe60a;', 'bf0749b074894312a7914a1ed90c07b4', 'y', '2018-08-06 15:58:06', null, 'admin', null);
INSERT INTO `t_sys_menu` VALUES ('59a31406fef84ec78bb3773a3336de01', '系统日志', 'sysLog', '/sys/log/list', '&#xe60a;', '8d458deec37c4a848a3a38982bc59f88', 'y', '2018-07-28 22:33:36', '2018-07-29 13:12:53', 'root', null);
INSERT INTO `t_sys_menu` VALUES ('5b417b62b27c43c09027af13eac7b1ca', '菜单管理', 'sysMenu', '/sys/menu/list', '&#xe60a;', '8d458deec37c4a848a3a38982bc59f87', 'y', '2018-07-22 18:16:44', '2018-07-23 11:16:18', 'root', null);
INSERT INTO `t_sys_menu` VALUES ('8d458deec37c4a848a3a38982bc59f87', '系统管理', 'sysMenu', '', '&#xe614;', null, 'y', '2018-07-21 17:06:57', '2018-07-28 22:22:27', 'root', null);
INSERT INTO `t_sys_menu` VALUES ('8d458deec37c4a848a3a38982bc59f88', '日志管理', 'sysLogs', '', '&#xe614;', null, 'y', '2018-07-21 17:06:57', '2018-08-06 11:02:50', 'root', null);
INSERT INTO `t_sys_menu` VALUES ('9bf96789a2c9404fbacad461bc84dc96', '角色管理', 'sysRole', '/sys/role/list', '&#xe60a;', '8d458deec37c4a848a3a38982bc59f87', 'y', '2018-07-29 13:20:59', '2018-07-31 10:00:48', 'root', null);
INSERT INTO `t_sys_menu` VALUES ('a5fc7f129ff44d48b369dfb07aa46eb2', '用户管理', 'sysUser', '/sys/user/list', '&#xe60a;', '8d458deec37c4a848a3a38982bc59f87', 'y', '2018-07-22 18:15:26', '2018-07-28 19:02:53', 'root', null);
INSERT INTO `t_sys_menu` VALUES ('bf0749b074894312a7914a1ed90c07b4', '文件管理', 'ftpFile', '', '&#xe614;', null, 'y', '2018-08-06 15:56:46', null, 'admin', null);


-- ----------------------------
-- Records of t_sys_role
-- ----------------------------
INSERT INTO `t_sys_role` VALUES ('85cbaa5711414aa2a3a853f246df7cd5', '超级管理', 'y', '2018-08-02 16:13:22', null, 'root', null);
INSERT INTO `t_sys_role` VALUES ('db43ef1d157145879ba4a29984a0ba76', '文件用户', 'y', '2018-08-06 16:01:54', null, 'admin', null);
INSERT INTO `t_sys_role` VALUES ('fc0c135a0e1845a799b4afcb5d7ec1dc', '系统管理', 'y', '2018-08-03 16:23:43', null, 'root', null);


-- ----------------------------
-- Records of t_sys_role_menu
-- ----------------------------
INSERT INTO `t_sys_role_menu` VALUES ('07d4a1c57f44450cbacaeb9ea66c389e', '85cbaa5711414aa2a3a853f246df7cd5', 'bf0749b074894312a7914a1ed90c07b4', '2018-08-06 16:18:27');
INSERT INTO `t_sys_role_menu` VALUES ('247ad96a18c6443b923af66c9f28cc78', '85cbaa5711414aa2a3a853f246df7cd5', '5b417b62b27c43c09027af13eac7b1ca', '2018-08-06 16:18:27');
INSERT INTO `t_sys_role_menu` VALUES ('368810acaa6e4f36969d0b3101b0c465', '85cbaa5711414aa2a3a853f246df7cd5', '9bf96789a2c9404fbacad461bc84dc96', '2018-08-06 16:18:27');
INSERT INTO `t_sys_role_menu` VALUES ('37087b1d08ea47139b541a97c2d21a45', 'db43ef1d157145879ba4a29984a0ba76', 'bf0749b074894312a7914a1ed90c07b4', '2018-08-06 16:02:02');
INSERT INTO `t_sys_role_menu` VALUES ('440737627c74406bb20baea5db07b3fb', 'fc0c135a0e1845a799b4afcb5d7ec1dc', '59a31406fef84ec78bb3773a3336de01', '2018-08-08 13:34:50');
INSERT INTO `t_sys_role_menu` VALUES ('4e2d1073178b4ee4bdd4f1fefb3b6d03', 'fc0c135a0e1845a799b4afcb5d7ec1dc', '19391d0a6035479fb4690f195e00faa5', '2018-08-08 13:34:50');
INSERT INTO `t_sys_role_menu` VALUES ('529fc2b3754c4d07b5bb9e6e96323713', '85cbaa5711414aa2a3a853f246df7cd5', '19391d0a6035479fb4690f195e00faa5', '2018-08-06 16:18:27');
INSERT INTO `t_sys_role_menu` VALUES ('5f23e3955fca45cfb67f49abeb6a9bda', '85cbaa5711414aa2a3a853f246df7cd5', '59a31406fef84ec78bb3773a3336de01', '2018-08-06 16:18:27');
INSERT INTO `t_sys_role_menu` VALUES ('5f6b38354dba49c88f7aadc35f4b01ad', 'fc0c135a0e1845a799b4afcb5d7ec1dc', '37665f54c51e4d1ca0f9afd3a2442c94', '2018-08-08 13:34:50');
INSERT INTO `t_sys_role_menu` VALUES ('7475b536929f40b0890a7cd9a5fcb0fe', 'fc0c135a0e1845a799b4afcb5d7ec1dc', 'a5fc7f129ff44d48b369dfb07aa46eb2', '2018-08-08 13:34:50');
INSERT INTO `t_sys_role_menu` VALUES ('7aea428bd027462494bfc0982ce39024', 'fc0c135a0e1845a799b4afcb5d7ec1dc', 'bf0749b074894312a7914a1ed90c07b4', '2018-08-08 13:34:50');
INSERT INTO `t_sys_role_menu` VALUES ('9cd1580362884e398c2dbb132e58c32e', 'db43ef1d157145879ba4a29984a0ba76', '37665f54c51e4d1ca0f9afd3a2442c94', '2018-08-06 16:02:02');
INSERT INTO `t_sys_role_menu` VALUES ('a40a6afc06f644df86aa0712956ff3b5', '85cbaa5711414aa2a3a853f246df7cd5', '8d458deec37c4a848a3a38982bc59f88', '2018-08-06 16:18:27');
INSERT INTO `t_sys_role_menu` VALUES ('a7589173304c47d59d04a6afa272338d', 'fc0c135a0e1845a799b4afcb5d7ec1dc', '8d458deec37c4a848a3a38982bc59f88', '2018-08-08 13:34:50');
INSERT INTO `t_sys_role_menu` VALUES ('c27ccaeb208f4d52ad059358759c6118', 'fc0c135a0e1845a799b4afcb5d7ec1dc', '9bf96789a2c9404fbacad461bc84dc96', '2018-08-08 13:34:50');
INSERT INTO `t_sys_role_menu` VALUES ('c88ba37aee05478fa41783906418821e', 'fc0c135a0e1845a799b4afcb5d7ec1dc', '5b417b62b27c43c09027af13eac7b1ca', '2018-08-08 13:34:50');
INSERT INTO `t_sys_role_menu` VALUES ('d19619b901484f5e806d5bd3382a4431', 'fc0c135a0e1845a799b4afcb5d7ec1dc', '8d458deec37c4a848a3a38982bc59f87', '2018-08-08 13:34:50');
INSERT INTO `t_sys_role_menu` VALUES ('dcfcd1fc8a014a39a82356c754608bd9', '85cbaa5711414aa2a3a853f246df7cd5', '8d458deec37c4a848a3a38982bc59f87', '2018-08-06 16:18:27');
INSERT INTO `t_sys_role_menu` VALUES ('de8fddda57eb47adb97baa7ec8a1f97a', '85cbaa5711414aa2a3a853f246df7cd5', '37665f54c51e4d1ca0f9afd3a2442c94', '2018-08-06 16:18:27');
INSERT INTO `t_sys_role_menu` VALUES ('fbc893386e7843d1a4ea3dff2590877b', '85cbaa5711414aa2a3a853f246df7cd5', 'a5fc7f129ff44d48b369dfb07aa46eb2', '2018-08-06 16:18:27');


-- ----------------------------
-- Records of t_sys_user
-- ----------------------------
INSERT INTO `t_sys_user` VALUES ('487d6d14c22d44e4a448c3ab60bd8b73', 'root', '$2a$10$uTvfFErGXXJyvEgRr3JR5eNK1zJwSHIYu70mcbpPT8Zevf6SC.SB2', '487d6d14c22d44e4a448c3ab60bd8b73', '1370581389@qq.com', '/image/head.png', '0', 'y', '2018-08-02 11:56:02', '2018-08-03 16:24:30', 'sys', null);
INSERT INTO `t_sys_user` VALUES ('487d6d14c22d44e4a448c3ab60bd8b74', 'admin', '$2a$10$uTvfFErGXXJyvEgRr3JR5eNK1zJwSHIYu70mcbpPT8Zevf6SC.SB2', '487d6d14c22d44e4a448c3ab60bd8b74', '1370581389@qq.com', '/image/head.png', '0', 'y', '2018-07-30 15:57:11', '2018-08-03 16:24:30', 'root', null);
INSERT INTO `t_sys_user` VALUES ('487d6d14c22d44e4a448c3ab60bd8b75', 'test', '$2a$10$PaIYzY8uHrIHQsno5aXtuee8at0TdfgVW8lg4YtTsqUTl81w1Z4la', '487d6d14c22d44e4a448c3ab60bd8b75', '1370581389@qq.com', '/image/head.png', '1', 'y', '2018-08-06 16:01:32', null, 'admin', null);


-- ----------------------------
-- Records of t_sys_user_role
-- ----------------------------
INSERT INTO `t_sys_user_role` VALUES ('43e1b953def54c4cabfa4d29d3a25489', 'db43ef1d157145879ba4a29984a0ba76', '487d6d14c22d44e4a448c3ab60bd8b75', '2018-08-06 16:02:13');
INSERT INTO `t_sys_user_role` VALUES ('5618f0d81d3949be84dfa05b2be144ab', '938aa2e8653547fab8b34d16e720f625', '487d6d14c22d44e4a448c3ab60bd8b73', '2018-08-02 15:18:43');
INSERT INTO `t_sys_user_role` VALUES ('f1bc2751c3ca4db7a350513c59499d6c', 'fc0c135a0e1845a799b4afcb5d7ec1dc', '487d6d14c22d44e4a448c3ab60bd8b74', '2018-08-06 16:18:53');
