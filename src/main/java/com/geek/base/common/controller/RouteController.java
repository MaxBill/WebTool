package com.geek.base.common.controller;

import com.alibaba.fastjson.JSON;
import com.geek.base.system.bean.SysUser;
import com.geek.base.system.service.SysMenuService;
import com.geek.base.system.service.SysUserService;
import com.geek.tool.DataUtil;
import com.geek.tool.SecurityUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

/**
 * @功能 系统路由控制器
 * @作者 MaxBill
 * @时间 2018年7月19日
 * @邮箱 maxbill1993@163.com
 */
@Controller
public class RouteController {

    @Autowired
    private SysMenuService sysMenuService;

    @RequestMapping("/root")
    public ModelAndView root(ModelAndView mv) {
        mv.addObject("userInfo", DataUtil.getUser());
        mv.addObject("menuList", this.sysMenuService.selectShowUserMenu(DataUtil.getUserId()));
        mv.setViewName("index");
        return mv;
    }

    @RequestMapping("/main")
    public ModelAndView main(ModelAndView mv) {
        mv.addObject("userInfo", DataUtil.getUser());
        mv.setViewName("main");
        return mv;
    }

    @RequestMapping("/auth")
    public ModelAndView login(ModelAndView mv) {
        if (SecurityUtil.isLogin()) {
            return root(mv);
        } else {
            mv.setViewName("login");
            return mv;
        }
    }

    @RequestMapping("/")
    public ModelAndView index(ModelAndView mv) {

        if (SecurityUtil.isLogin()) {
            return root(mv);
        } else {
            mv.setViewName("login");
            return mv;
        }
    }

}
