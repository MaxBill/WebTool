package com.geek.base.common.bean;

import com.geek.base.system.bean.SysMenu;
import lombok.Data;

import java.util.List;

@Data
public class MenuBean {

    //菜单名称
    private String menuName;

    //菜单地址
    private String menuPath;

    //菜单图标
    private String menuIcon;

    //下级菜单
    private List<SysMenu> children;
}
