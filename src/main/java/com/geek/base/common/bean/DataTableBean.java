package com.geek.base.common.bean;

import lombok.Data;

@Data
public class DataTableBean {

    private Integer code;

    private String msgs;

    private Long count;

    private Object data;

    private Object param;
}
