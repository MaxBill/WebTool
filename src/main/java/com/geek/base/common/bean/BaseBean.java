package com.geek.base.common.bean;

import lombok.Data;

import java.util.Date;

/**
 * @功能 父级实体
 * @作者 MaxBill
 * @时间 2018年7月20日
 * @邮箱 maxbill1993@163.com
 */
@Data
public class BaseBean {

    //创建时间
    private Date createTime;

    //修改时间
    private Date updateTime;

    //创建人
    private String createUser;

    //修改人
    private String updateUser;

}
