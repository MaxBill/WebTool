package com.geek.base.common.enums;

public enum LogType {

    MENU_LOG("ML"),
    USER_LOG("UL"),
    ROLE_LOG("RL"),
    ELSE_LOG("EL");

    private String value;

    LogType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return this.getValue();
    }

}
