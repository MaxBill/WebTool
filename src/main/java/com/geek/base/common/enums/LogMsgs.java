package com.geek.base.common.enums;

public enum LogMsgs {

    //系统模块日志

    MENU_LIST("打开菜单列表页面"),
    MENU_SAVE("打开菜单新增页面"),
    MENU_EDIT("打开菜单编辑页面"),

    ROLE_LIST("打开角色列表页面"),
    ROLE_SAVE("打开角色新增页面"),
    ROLE_EDIT("打开角色编辑页面"),
    ROLE_MENU("打开角色菜单页面"),

    USER_LIST("打开用户列表页面"),
    USER_SAVE("打开用户新增页面"),
    USER_EDIT("打开用户编辑页面"),
    USER_ROLE("打开用户角色页面"),
    USER_PASS("打开修改密码页面"),
    USER_INFO("打开用户信息页面"),
    USER_DEAL("系统用户登录操作"),

    MENU_MYTREE("查询树形菜单操作"),
    MENU_SELECT("查询系统菜单操作"),
    MENU_INSERT("新增系统菜单操作"),
    MENU_UPDATE("编辑系统菜单操作"),
    MENU_DELETE("删除系统菜单操作"),

    ROLE_CONFIG("配置角色菜单操作"),
    ROLE_SELECT("查询系统角色操作"),
    ROLE_INSERT("新增系统角色操作"),
    ROLE_UPDATE("编辑系统角色操作"),
    ROLE_DELETE("删除系统角色操作"),

    USER_CONFIG("配置用户角色操作"),
    USER_SELECT("查询系统用户操作"),
    USER_INSERT("新增系统用户操作"),
    USER_UPDATE("编辑系统用户操作"),
    USER_DELETE("删除系统用户操作"),
    USER_CHANGE("用户修改密码操作"),


    //业务模块日志

    FTP0_LIST("打开协议列表页面"),
    FTP0_SAVE("打开协议新增页面"),
    FTP0_EDIT("打开协议编辑页面"),
    FTP0_LOOK("打开协议详情页面"),
    FTP0_FILE("打开文件列表页面"),
    FTP0_PICK("打开文件上传页面"),
    FTP0_COPY("打开文件复制页面"),
    FTP0_MOVE("打开文件移动页面"),


    FTP0_SELECT("查询文件协议操作"),
    FTP0_INSERT("新增文件协议操作"),
    FTP0_UPDATE("编辑文件协议操作"),
    FTP0_DELETE("删除文件协议操作"),


    FTP3_LSFILE("获取目录列表操作"),
    FTP3_MKPATH("创建文件目录操作"),
    FTP3_RMFILE("删除目录文件操作"),
    FTP3_RENAME("命名文件目录操作"),
    FTP3_UPLOAD("上传目录文件操作"),
    FTP3_MYTREE("获取目录树的操作"),
    FTP3_DOCOPY("复制目录文件操作"),
    FTP3_DOMOVE("移动目录文件操作"),
    FTP3_DODOWN("下载目录文件操作");


    private String value;

    LogMsgs(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

}
