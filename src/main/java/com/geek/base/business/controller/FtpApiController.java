package com.geek.base.business.controller;

import com.geek.base.business.bean.FileBean;
import com.geek.base.common.bean.DataTableBean;
import com.geek.base.common.bean.ResponseBean;
import com.geek.base.common.enums.LogMsgs;
import com.geek.base.common.enums.LogType;
import com.geek.core.config.annotation.WebLog;
import com.geek.tool.DataUtil;
import com.geek.tool.FtpPoolUtil;
import org.apache.commons.net.ftp.FTPClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author zhaoyu
 * @date 2018/8/8 17:47
 * @email 17521331369@163.com
 */
@Controller
@RequestMapping("/rtb/ftp")
public class FtpApiController {

    @Autowired
    private DataUtil dataUtil;

    @ResponseBody
    @RequestMapping("/lsfile")
    public DataTableBean lsfile(String ftpId, Boolean showFile, String filePath) {
        DataTableBean tableData = new DataTableBean();
        try {
            Map<String, Object> resultMap = new HashMap<>(50);
            FTPClient ftpClient = dataUtil.getFTPClient(ftpId);
            // 文件列表
            List<FileBean> fileList = FtpPoolUtil.listFiles(filePath, ftpClient);
            tableData.setCode(200);
            tableData.setCount((long) fileList.size());
            tableData.setMsgs("");
            tableData.setData(fileList);
            resultMap.put("showFile", showFile);
            resultMap.put("homePath", FtpPoolUtil.getHomePath());
            resultMap.put("lastPath", FtpPoolUtil.getLastPath(filePath));
            resultMap.put("currPath", FtpPoolUtil.getCurrPath(filePath));
            tableData.setParam(resultMap);
        } catch (Exception e) {
            tableData.setCode(500);
            tableData.setMsgs(e.getMessage());
            e.printStackTrace();
        }
        return tableData;
    }

    @ResponseBody
    @RequestMapping("/rename")
    @WebLog(msgs = LogMsgs.FTP3_RENAME, type = LogType.ELSE_LOG)
    public ResponseBean rename(String ftpId, String currPath, String fromFileName, String toFileName) {
        ResponseBean responseBean = new ResponseBean();
        if (fromFileName.equals(toFileName)) {
            responseBean.setMsgs("重命名成功");
            return responseBean;
        }
        if (StringUtils.isEmpty(currPath)) {
            currPath = FtpPoolUtil.getHomePath();
        }
        try {
            FTPClient ftpClient = dataUtil.getFTPClient(ftpId);
            if (FtpPoolUtil.rename(currPath, fromFileName, toFileName, ftpClient)) {
                responseBean.setMsgs("重命名成功");
            } else {
                responseBean.setMsgs("重命名失败");
                responseBean.setCode(0);
            }
        } catch (Exception e) {
            responseBean.setMsgs("重命名失败");
            e.printStackTrace();
            responseBean.setCode(0);
        }
        return responseBean;
    }

}
