package com.geek.base.business.controller;

import com.geek.base.business.bean.FtpInfo;
import com.geek.base.business.service.FtpInfoService;
import com.geek.base.common.bean.DataTableBean;
import com.geek.base.common.bean.ResponseBean;
import com.geek.base.common.enums.LogMsgs;
import com.geek.base.common.enums.LogType;
import com.geek.core.config.annotation.WebLog;
import com.geek.tool.*;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.jcraft.jsch.ChannelSftp;
import org.apache.commons.net.ftp.FTPClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/rtb/ftp")
public class FtpInfoController {

    @Autowired
    private FtpInfoService ftpInfoService;

    @RequestMapping("/list")
    @WebLog(msgs = LogMsgs.FTP0_LIST, type = LogType.ELSE_LOG)
    public String listFtp() {
        return "business/ftp_list";
    }

    @RequestMapping("/save")
    @WebLog(msgs = LogMsgs.FTP0_SAVE, type = LogType.ELSE_LOG)
    public String saveFtp() {
        return "business/ftp_save";
    }

    @RequestMapping("/edit")
    @WebLog(msgs = LogMsgs.FTP0_EDIT, type = LogType.ELSE_LOG)
    public ModelAndView editFtp(ModelAndView mv, String ftpId) {
        FtpInfo ftpInfo = this.ftpInfoService.selectFtpInfoByFtpId(ftpId);
        mv.addObject("ftpInfo", ftpInfo);
        mv.setViewName("business/ftp_edit");
        return mv;
    }

    @RequestMapping("/look")
    @WebLog(msgs = LogMsgs.FTP0_LOOK, type = LogType.ELSE_LOG)
    public ModelAndView lookFtp(ModelAndView mv, String ftpId) {
        FtpInfo ftpInfo = this.ftpInfoService.selectFtpInfoByFtpId(ftpId);
        mv.addObject("ftpInfo", ftpInfo);
        mv.setViewName("business/ftp_look");
        return mv;
    }

    @RequestMapping("/file")
    @WebLog(msgs = LogMsgs.FTP0_FILE, type = LogType.ELSE_LOG)
    public ModelAndView listFile(ModelAndView mv, String ftpId, String ftpType) {
        try {
            // 三种协议的不同处理逻辑
            switch (ftpType) {
                case "1":
                    FTPClient ftpClient = (FTPClient) HttpWebUtil.getSessionAttribute("ftpObj");
                    if (null == ftpClient || !ftpClient.isConnected()) {
                        FtpInfo ftpInfo = this.ftpInfoService.selectFtpInfoByFtpId(ftpId);
                        ftpClient = FtpPoolUtil.getFtpClient(ftpInfo);
                        Map<String, Object> sessionMap = new HashMap<>(50);
                        sessionMap.put("ftpInfo", ftpInfo);
                        sessionMap.put("ftpObj", ftpClient);
                        HttpWebUtil.setSessionAttribute(ftpId, sessionMap);
                    }
                    break;
                case "2":
                    break;
                case "3":
                    ChannelSftp sftp = (ChannelSftp) HttpWebUtil.getSessionAttribute("sftpObj");
                    if (null == sftp || !sftp.isConnected()) {
                        FtpInfo ftpInfo = this.ftpInfoService.selectFtpInfoByFtpId(ftpId);
                        sftp = SftpPoolUtil.openSftpConnect(ftpInfo);
                        Map<String, Object> sessionMap = new HashMap<>();
                        sessionMap.put("ftpInfo", ftpInfo);
                        sessionMap.put("sftpObj", sftp);
                        HttpWebUtil.setSessionAttribute(ftpId, sessionMap);
                    }
                    break;
                default:
                    break;
            }
        } catch (Exception e) {
        }
        mv.addObject("ftpId", ftpId);
        mv.addObject("ftpType", ftpType);
        mv.setViewName("business/ftp_file");
        return mv;
    }

    @RequestMapping("/pick")
    @WebLog(msgs = LogMsgs.FTP0_PICK, type = LogType.ELSE_LOG)
    public ModelAndView pickFile(ModelAndView mv, String ftpId, String ftpType, String currPath) {
        mv.addObject("ftpId", ftpId);
        mv.addObject("ftpType", ftpType);
        mv.addObject("currPath", currPath);
        mv.setViewName("business/ftp_upload");
        return mv;
    }

    @RequestMapping("/copy")
    @WebLog(msgs = LogMsgs.FTP0_COPY, type = LogType.ELSE_LOG)
    public ModelAndView copyFile(ModelAndView mv, String ftpId, String ftpType, String filePath) {
        mv.addObject("ftpId", ftpId);
        mv.addObject("ftpType", ftpType);
        mv.addObject("filePath", filePath);
        mv.setViewName("business/ftp_copy");
        return mv;
    }

    @RequestMapping("/move")
    @WebLog(msgs = LogMsgs.FTP0_MOVE, type = LogType.ELSE_LOG)
    public ModelAndView moveFile(ModelAndView mv, String ftpId, String ftpType, String filePath) {
        mv.addObject("ftpId", ftpId);
        mv.addObject("ftpType", ftpType);
        mv.addObject("filePath", filePath);
        mv.setViewName("business/ftp_move");
        return mv;
    }

    @ResponseBody
    @RequestMapping("/insert")
    @WebLog(msgs = LogMsgs.FTP0_INSERT, type = LogType.ELSE_LOG)
    public ResponseBean insertFtp(FtpInfo ftpInfo) {
        ResponseBean responseBean = new ResponseBean();
        ftpInfo.setFtpId(KeyGeneratorUtil.getUUIDKey());
        ftpInfo.setCreateUser(DataUtil.getUserName());
        ftpInfo.setCreateTime(new Date());
        int insFlag = ftpInfoService.insertFtpInfo(ftpInfo);
        if (insFlag == 1) {
            responseBean.setMsgs("新增FTP成功");
        } else {
            responseBean.setMsgs("新增FTP失败");
        }
        return responseBean;
    }

    @ResponseBody
    @RequestMapping("/update")
    @WebLog(msgs = LogMsgs.FTP0_UPDATE, type = LogType.ELSE_LOG)
    public ResponseBean updateFtp(FtpInfo ftpInfo) {
        ResponseBean responseBean = new ResponseBean();
        ftpInfo.setUpdateUser(DataUtil.getUserName());
        ftpInfo.setUpdateTime(new Date());
        int updFlag = ftpInfoService.updateFtpInfo(ftpInfo);
        if (updFlag == 1) {
            responseBean.setMsgs("编辑FTP成功");
        } else {
            responseBean.setMsgs("编辑FTP失败");
        }
        return responseBean;
    }

    @ResponseBody
    @RequestMapping("/delete")
    @WebLog(msgs = LogMsgs.FTP0_DELETE, type = LogType.ELSE_LOG)
    public ResponseBean deleteFtp(String ftpId) {
        ResponseBean responseBean = new ResponseBean();
        int delFlag = ftpInfoService.deleteFtpInfo(ftpId);
        if (delFlag == 1) {
            responseBean.setMsgs("删除FTP成功");
        } else {
            responseBean.setMsgs("删除FTP失败");
        }
        return responseBean;
    }


    @ResponseBody
    @RequestMapping("/select")
    @WebLog(msgs = LogMsgs.FTP0_SELECT, type = LogType.ELSE_LOG)
    public DataTableBean selectFtp(FtpInfo ftpInfo, int page, int limit) {
        DataTableBean tableData = new DataTableBean();
        Page<FtpInfo> pager = PageHelper.startPage(page, limit);
        List<FtpInfo> ftpInfoList = this.ftpInfoService.selectFtpInfoList(ftpInfo);
        tableData.setCode(200);
        tableData.setCount(pager.getTotal());
        tableData.setMsgs("");
        tableData.setData(ftpInfoList);
        return tableData;
    }

}
