package com.geek.base.business.service;

import com.geek.base.business.bean.FtpInfo;

import java.util.List;

public interface FtpInfoService {

    int insertFtpInfo(FtpInfo record);

    int updateFtpInfo(FtpInfo record);

    int deleteFtpInfo(String ftpId);

    FtpInfo selectFtpInfoByFtpId(String ftpId);

    List<FtpInfo> selectFtpInfoList(FtpInfo record);
}
