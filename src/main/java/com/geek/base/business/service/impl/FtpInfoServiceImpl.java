package com.geek.base.business.service.impl;

import com.geek.base.business.bean.FtpInfo;
import com.geek.base.business.dao.FtpInfoMapper;
import com.geek.base.business.service.FtpInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FtpInfoServiceImpl implements FtpInfoService {

    @Autowired
    private FtpInfoMapper ftpInfoMapper;

    public int insertFtpInfo(FtpInfo record){
        return this.ftpInfoMapper.insert(record);
    }

    public int updateFtpInfo(FtpInfo record){
        return this.ftpInfoMapper.update(record);
    }

    public int deleteFtpInfo(String ftpId){
        return this.ftpInfoMapper.delete(ftpId);
    }

    public FtpInfo selectFtpInfoByFtpId(String ftpId){
        return this.ftpInfoMapper.selectFtpInfoByFtpId(ftpId);
    }

    public List<FtpInfo> selectFtpInfoList(FtpInfo record) {
        return this.ftpInfoMapper.selectFtpInfoList(record);
    }

}
