package com.geek.base.business.dao;

import com.geek.base.business.bean.FtpInfo;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FtpInfoMapper {

    int insert(FtpInfo record);

    int update(FtpInfo record);

    int delete(String ftpId);

    FtpInfo selectFtpInfoByFtpId(String ftpId);

    List<FtpInfo> selectFtpInfoList(FtpInfo record);

}
