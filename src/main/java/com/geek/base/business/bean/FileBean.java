package com.geek.base.business.bean;

import lombok.Data;

@Data
public class FileBean {

    private String filePath;

    private String fileName;

    private boolean fileType;

    private long fileSize;

    private String accessTime;

    private String modifyTime;

}
