package com.geek.base.system.controller;

import com.geek.base.common.bean.DataTableBean;
import com.geek.base.common.bean.ResponseBean;
import com.geek.base.common.bean.ZTreeBean;
import com.geek.base.common.enums.LogMsgs;
import com.geek.base.common.enums.LogType;
import com.geek.base.system.bean.SysMenu;
import com.geek.base.system.service.SysMenuService;
import com.geek.core.config.annotation.WebLog;
import com.geek.tool.DataUtil;
import com.geek.tool.KeyGeneratorUtil;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.Date;
import java.util.List;

/**
 * @功能 系统菜单控制器
 * @作者 MaxBill
 * @时间 2018年7月19日
 * @邮箱 maxbill1993@163.com
 */
@Controller
@RequestMapping("/sys/menu")
public class SysMenuController {

    @Autowired
    private SysMenuService sysMenuService;


    @RequestMapping("/list")
    @WebLog(msgs = LogMsgs.MENU_LIST, type = LogType.MENU_LOG)
    public String listMenu() {
        return "system/menu_list";
    }

    @RequestMapping("/save")
    @WebLog(msgs = LogMsgs.MENU_SAVE, type = LogType.MENU_LOG)
    public ModelAndView saveMenu(ModelAndView mv) {
        List<SysMenu> parentMenuList = this.sysMenuService.selectParentMenuList();
        mv.addObject("parentMenuList", parentMenuList);
        mv.setViewName("system/menu_save");
        return mv;
    }

    @RequestMapping("/edit")
    @WebLog(msgs = LogMsgs.MENU_EDIT, type = LogType.MENU_LOG)
    public ModelAndView editMenu(ModelAndView mv, String menuId) {
        List<SysMenu> parentMenuList = this.sysMenuService.selectParentMenuList();
        mv.addObject("parentMenuList", parentMenuList);
        SysMenu sysMenu = this.sysMenuService.selectMenuByMenuId(menuId);
        mv.addObject("sysMenu", sysMenu);
        mv.setViewName("system/menu_edit");
        return mv;
    }

    @ResponseBody
    @RequestMapping("/mytree")
    @WebLog(msgs = LogMsgs.MENU_MYTREE, type = LogType.MENU_LOG)
    public ResponseBean treeMenu(String roleId) {
        ResponseBean responseBean = new ResponseBean();
        List<String> checkTreeList = this.sysMenuService.selectCheckMenuList(roleId);
        List<ZTreeBean> totleTreeList = this.sysMenuService.selectMenuTreeList();
        if (null != checkTreeList && null != totleTreeList) {
            for (ZTreeBean treeBean : totleTreeList) {
                if (checkTreeList.contains(treeBean.getId())) {
                    treeBean.setChecked(true);
                }
                if(null==treeBean.getPId()){
                    treeBean.setParent(true);
                }
            }
        }
        responseBean.setData(totleTreeList);
        return responseBean;
    }

    @ResponseBody
    @RequestMapping("/insert")
    @WebLog(msgs = LogMsgs.MENU_INSERT, type = LogType.MENU_LOG)
    public ResponseBean insertMenu(SysMenu menu) {
        ResponseBean responseBean = new ResponseBean();
        menu.setMenuId(KeyGeneratorUtil.getUUIDKey());
        String isShow = menu.getMenuShow();
        if (!StringUtils.isEmpty(isShow) && "on".equals(isShow)) {
            menu.setMenuShow("y");
        } else {
            menu.setMenuShow("n");
        }
        //给默认图标
        if (StringUtils.isEmpty(menu.getMenuPid())) {
            //一级菜单
            menu.setMenuIcon("&#xe614;");
        } else {
            //二级菜单
            menu.setMenuIcon("&#xe60a;");
        }
        menu.setCreateUser(DataUtil.getUserName());
        menu.setCreateTime(new Date());
        int insFlag = sysMenuService.insertMenu(menu);
        if (insFlag == 1) {
            responseBean.setMsgs("新增菜单成功");
        } else {
            responseBean.setMsgs("新增菜单失败");
        }
        return responseBean;
    }


    @ResponseBody
    @RequestMapping("/update")
    @WebLog(msgs = LogMsgs.MENU_UPDATE, type = LogType.MENU_LOG)
    public ResponseBean updateMenu(SysMenu menu) {
        ResponseBean responseBean = new ResponseBean();
        String isShow = menu.getMenuShow();
        if (!StringUtils.isEmpty(isShow) && "on".equals(isShow)) {
            menu.setMenuShow("y");
        } else {
            menu.setMenuShow("n");
        }
        menu.setUpdateUser(DataUtil.getUserName());
        menu.setUpdateTime(new Date());
        int updFlag = sysMenuService.updateMenu(menu);
        if (updFlag == 1) {
            responseBean.setMsgs("编辑菜单成功");
        } else {
            responseBean.setMsgs("编辑菜单失败");
        }
        return responseBean;
    }


    @ResponseBody
    @RequestMapping("/delete")
    @WebLog(msgs = LogMsgs.MENU_DELETE, type = LogType.MENU_LOG)
    public ResponseBean deleteMenu(String menuId) {
        ResponseBean responseBean = new ResponseBean();
        int insFlag = sysMenuService.deleteMenu(menuId);
        if (insFlag == 1) {
            responseBean.setMsgs("删除菜单成功");
        } else {
            responseBean.setMsgs("删除菜单失败");
        }
        return responseBean;
    }

    @ResponseBody
    @RequestMapping("/select")
    @WebLog(msgs = LogMsgs.MENU_SELECT, type = LogType.MENU_LOG)
    public DataTableBean selectMenu(SysMenu menu, int page, int limit) {
        DataTableBean tableData = new DataTableBean();
        Page<SysMenu> pager = PageHelper.startPage(page, limit);
        List<SysMenu> menuList = this.sysMenuService.selectMenuList(menu);
        tableData.setCode(200);
        tableData.setCount(pager.getTotal());
        tableData.setMsgs("");
        tableData.setData(menuList);
        return tableData;
    }

}
