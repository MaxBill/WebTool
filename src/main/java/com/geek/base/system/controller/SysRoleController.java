package com.geek.base.system.controller;

import com.geek.base.common.bean.DataTableBean;
import com.geek.base.common.bean.ResponseBean;
import com.geek.base.common.enums.LogMsgs;
import com.geek.base.common.enums.LogType;
import com.geek.base.system.bean.SysRole;
import com.geek.base.system.service.SysRoleService;
import com.geek.core.config.annotation.WebLog;
import com.geek.tool.DataUtil;
import com.geek.tool.KeyGeneratorUtil;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.Date;
import java.util.List;

/**
 * @功能 系统角色控制器
 * @作者 MaxBill
 * @时间 2018年7月19日
 * @邮箱 maxbill1993@163.com
 */
@Controller
@RequestMapping("/sys/role")
public class SysRoleController {

    @Autowired
    private SysRoleService sysRoleService;


    @RequestMapping("/list")
    @WebLog(msgs = LogMsgs.ROLE_LIST, type = LogType.ROLE_LOG)
    public String listRole() {
        return "system/role_list";
    }

    @RequestMapping("/save")
    @WebLog(msgs = LogMsgs.ROLE_SAVE, type = LogType.ROLE_LOG)
    public String saveRole() {
        return "system/role_save";
    }


    @RequestMapping("/edit")
    @WebLog(msgs = LogMsgs.ROLE_EDIT, type = LogType.ROLE_LOG)
    public ModelAndView editRole(ModelAndView mv, String roleId) {
        SysRole sysRole = this.sysRoleService.selectRoleByRoleId(roleId);
        mv.addObject("sysRole", sysRole);
        mv.setViewName("system/role_edit");
        return mv;
    }

    @RequestMapping("/menu")
    @WebLog(msgs = LogMsgs.ROLE_MENU, type = LogType.ROLE_LOG)
    public ModelAndView menuRole(ModelAndView mv, String roleId) {
        mv.addObject("roleId", roleId);
        mv.setViewName("system/role_menu");
        return mv;
    }

    @ResponseBody
    @RequestMapping("/config")
    @WebLog(msgs = LogMsgs.ROLE_CONFIG, type = LogType.ROLE_LOG)
    public ResponseBean configRole(String roleId, String roleMenu) {
        ResponseBean responseBean = new ResponseBean();
        boolean insFlag = this.sysRoleService.configRole(roleId, roleMenu);
        if (insFlag) {
            responseBean.setMsgs("角色菜单授权成功");
        } else {
            responseBean.setMsgs("角色菜单授权失败");
        }
        return responseBean;
    }

    @ResponseBody
    @RequestMapping("/insert")
    @WebLog(msgs = LogMsgs.ROLE_INSERT, type = LogType.ROLE_LOG)
    public ResponseBean insertRole(SysRole role) {
        ResponseBean responseBean = new ResponseBean();
        role.setRoleId(KeyGeneratorUtil.getUUIDKey());

        String isUse = role.getRoleType();
        if (!StringUtils.isEmpty(isUse) && "on".equals(isUse)) {
            role.setRoleType("y");
        } else {
            role.setRoleType("n");
        }
        role.setCreateTime(new Date());
        role.setCreateUser(DataUtil.getUserName());
        int insFlag = this.sysRoleService.insertRole(role);
        if (insFlag == 1) {
            responseBean.setMsgs("新增角色成功");
        } else {
            responseBean.setMsgs("新增角色失败");
        }
        return responseBean;
    }

    @ResponseBody
    @RequestMapping("/update")
    @WebLog(msgs = LogMsgs.ROLE_UPDATE, type = LogType.ROLE_LOG)
    public ResponseBean updateRole(SysRole role) {
        ResponseBean responseBean = new ResponseBean();
        String isUse = role.getRoleType();
        if (!StringUtils.isEmpty(isUse) && "on".equals(isUse)) {
            role.setRoleType("y");
        } else {
            role.setRoleType("n");
        }
        role.setUpdateUser(DataUtil.getUserName());
        role.setUpdateTime(new Date());
        int updFlag = this.sysRoleService.updateRole(role);
        if (updFlag == 1) {
            responseBean.setMsgs("编辑角色成功");
        } else {
            responseBean.setMsgs("编辑角色失败");
        }
        return responseBean;
    }


    @ResponseBody
    @RequestMapping("/delete")
    @WebLog(msgs = LogMsgs.ROLE_DELETE, type = LogType.ROLE_LOG)
    public ResponseBean deleteRole(String roleId) {
        ResponseBean responseBean = new ResponseBean();
        int delFlag = this.sysRoleService.deleteRole(roleId);
        if (delFlag == 1) {
            responseBean.setMsgs("删除角色成功");
        } else {
            responseBean.setMsgs("删除角色失败");
        }
        return responseBean;
    }

    @ResponseBody
    @RequestMapping("/select")
    @WebLog(msgs = LogMsgs.ROLE_SELECT, type = LogType.ROLE_LOG)
    public DataTableBean selectMenu(SysRole role, int page, int limit) {
        DataTableBean tableData = new DataTableBean();
        Page<SysRole> pager = PageHelper.startPage(page, limit);
        List<SysRole> roleList = this.sysRoleService.selectRoleList(role);
        tableData.setCode(200);
        tableData.setCount(pager.getTotal());
        tableData.setMsgs("");
        tableData.setData(roleList);
        return tableData;
    }


}
