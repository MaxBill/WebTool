package com.geek.base.system.controller;

import com.geek.base.common.bean.DataTableBean;
import com.geek.base.system.bean.SysLog;
import com.geek.base.system.service.SysLogService;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * @功能 系统日志控制器
 * @作者 MaxBill
 * @时间 2018年7月19日
 * @邮箱 maxbill1993@163.com
 */
@Controller
@RequestMapping("/sys/log")
public class SysLogController {

    @Autowired
    private SysLogService sysLogService;

    @RequestMapping("/list")
    public String listLog() {
        return "system/log_list";
    }

    @RequestMapping("/look")
    public ModelAndView lookLog(ModelAndView mv, String logId) {
        mv.addObject("sysLog", this.sysLogService.selectLogByLogId(logId));
        mv.setViewName("system/log_look");
        return mv;
    }

    @ResponseBody
    @RequestMapping("/select")
    public DataTableBean selectLog(SysLog log, int page, int limit) {
        DataTableBean tableData = new DataTableBean();
        Page<SysLog> pager =PageHelper.startPage(page, limit);
        List<SysLog> roleList = this.sysLogService.selectLogList(log);
        tableData.setCode(200);
        tableData.setCount(pager.getTotal());
        tableData.setMsgs("");
        tableData.setData(roleList);
        return tableData;
    }

}
