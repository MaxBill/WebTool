package com.geek.base.system.dao;

import com.geek.base.system.bean.SysUser;
import com.geek.base.system.bean.SysUserRole;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SysUserMapper {


    int insert(SysUser record);

    int update(SysUser record);

    int delete(String userId);

    int config(SysUserRole record);

    int deleteUserRole(String userId);

    List<SysUser> selectUserList(SysUser record);

    SysUser selectUserByUserId(String userId);

    SysUser selectUserByUserName(String userName);

    SysUserRole selectUserRoleByUserId(String userId);

}
