package com.geek.base.system.dao;

import com.geek.base.common.bean.MenuBean;
import com.geek.base.common.bean.ZTreeBean;
import com.geek.base.system.bean.SysMenu;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SysMenuMapper {

    int insert(SysMenu record);

    int update(SysMenu record);

    int delete(String menuId);

    List<SysMenu> selectMenuList(SysMenu record);

    List<SysMenu> selectParentMenuList();

    List<MenuBean> selectShowUserMenu(String userId);

    SysMenu selectMenuByMenuId(String menuId);

    List<ZTreeBean> selectMenuTreeList();

    List<String> selectCheckMenuList(String roleId);


}
