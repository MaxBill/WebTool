package com.geek.base.system.dao;

import com.geek.base.system.bean.SysRole;
import com.geek.base.system.bean.SysRoleMenu;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SysRoleMapper {

    int insert(SysRole record);

    int update(SysRole record);

    int delete(String roleId);

    int config(SysRoleMenu record);

    SysRole selectRoleByRoleId(String roleId);

    List<SysRole> selectRoleList(SysRole record);

    int deleteRoleMenu(String roleId);

    SysRole selectRoleByUserId(String userId);

}
