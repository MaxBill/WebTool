package com.geek.base.system.dao;

import com.geek.base.system.bean.SysLog;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SysLogMapper {

    int insert(SysLog record);

    SysLog selectLogByLogId(String logId);

    List<SysLog> selectLogList(SysLog record);

}
