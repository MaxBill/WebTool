package com.geek.base.system.bean;

import com.geek.base.common.bean.BaseBean;
import lombok.Data;

import java.util.List;

/**
 * @功能 角色实体
 * @作者 MaxBill
 * @时间 2018年7月29日
 * @邮箱 maxbill1993@163.com
 */
@Data
public class SysRole extends BaseBean {

    //角色ID
    private String roleId;

    //角色名称
    private String roleName;

    //是否启用，0：不启用，1：启用
    private String roleType;

    //角色菜单列表
    private List<SysMenu> menuList;

}
