package com.geek.base.system.bean;

import com.geek.base.common.bean.BaseBean;
import lombok.Data;

@Data
public class SysUserRole extends BaseBean {

    //用户角色ID
    private String urId;

    //用户ID
    private String userId;

    //角色ID
    private String roleId;


}
