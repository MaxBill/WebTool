package com.geek.base.system.bean;

import com.geek.base.common.bean.BaseBean;
import lombok.Data;

@Data
public class SysRoleMenu extends BaseBean {

    //角色菜单ID
    private String rmId;

    //角色ID
    private String roleId;

    //菜单ID
    private String menuId;

}
