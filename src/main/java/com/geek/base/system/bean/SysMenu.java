package com.geek.base.system.bean;

import com.geek.base.common.bean.BaseBean;
import lombok.Data;

import java.util.List;

/**
 * @功能 菜单实体
 * @作者 MaxBill
 * @时间 2018年7月20日
 * @邮箱 maxbill1993@163.com
 */
@Data
public class SysMenu extends BaseBean {

    //菜单ID
    private String menuId;

    //父级ID
    private String menuPid;

    //菜单名称
    private String menuName;

    //菜单标示
    private String menuCode;

    //菜单地址
    private String menuPath;

    //菜单图标
    private String menuIcon;

    //是否显示
    private String menuShow;

    //下级菜单
    private List<SysMenu> children;

}
