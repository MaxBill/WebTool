package com.geek.base.system.bean;

import com.geek.base.common.bean.BaseBean;
import lombok.Data;

import java.util.Date;

/**
 * @功能 日志实体
 * @作者 MaxBill
 * @时间 2018年7月29日
 * @邮箱 maxbill1993@163.com
 */
@Data
public class SysLog {

    //日志ID
    private String logId;

    //日志用户
    private String logUser;

    //日志名称
    private String logName;

    //日志地址
    private String logUrls;

    //日志路径
    private String logPath;

    //日志IP
    private String logAddr;

    //日志区域
    private String logArea;

    //日志时间
    private Date logTime;

    //日志类型，0：系统日志，1：用户日志，2：其他日志
    private String logType;

    //系统
    private String LogSystem;

    //请求方式
    private String LogMethod;

    //浏览器
    private String logBrowser;

    //请求数据
    private String logReqParam;

    //响应数据
    private String logResParam;
}
