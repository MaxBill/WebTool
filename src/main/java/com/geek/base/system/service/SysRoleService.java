package com.geek.base.system.service;

import com.geek.base.system.bean.SysRole;

import java.util.List;

public interface SysRoleService {

    int insertRole(SysRole record);

    int updateRole(SysRole record);

    int deleteRole(String roleId);

    boolean configRole(String roleId, String roleMenu);

    SysRole selectRoleByRoleId(String roleId);

    List<SysRole> selectRoleList(SysRole record);

    SysRole selectRoleByUserId(String userId);

}
