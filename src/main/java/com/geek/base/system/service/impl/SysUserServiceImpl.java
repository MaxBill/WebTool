package com.geek.base.system.service.impl;

import com.geek.base.system.bean.SysUser;
import com.geek.base.system.bean.SysUserRole;
import com.geek.base.system.dao.SysUserMapper;
import com.geek.base.system.service.SysUserService;
import com.geek.tool.KeyGeneratorUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * @功能 系统用户业务实现
 * @作者 MaxBill
 * @时间 2018年7月29日
 * @邮箱 maxbill1993@163.com
 */
@Service
public class SysUserServiceImpl implements SysUserService {

    @Autowired
    private SysUserMapper sysUserMapper;

    public int insertUser(SysUser record) {
        return this.sysUserMapper.insert(record);
    }

    public int updateUser(SysUser record) {
        return this.sysUserMapper.update(record);
    }

    @Transactional
    public int deleteUser(String userId) {
        int delFlag = this.sysUserMapper.delete(userId);
        if (delFlag == 1) {
            this.sysUserMapper.deleteUserRole(userId);
        }
        return delFlag;
    }

    @Transactional
    public int configUser(String userId, String roleId) {
        this.sysUserMapper.deleteUserRole(userId);
        SysUserRole userRole = new SysUserRole();
        userRole.setUrId(KeyGeneratorUtil.getUUIDKey());
        userRole.setUserId(userId);
        userRole.setRoleId(roleId);
        userRole.setCreateTime(new Date());
        return this.sysUserMapper.config(userRole);
    }

    public List<SysUser> selectUserList(SysUser record) {
        return this.sysUserMapper.selectUserList(record);
    }

    public SysUser selectUserByUserId(String userId) {
        return this.sysUserMapper.selectUserByUserId(userId);
    }

    public SysUser selectUserByUserName(String userName) {
        return this.sysUserMapper.selectUserByUserName(userName);
    }

    public SysUserRole selectUserRoleByUserId(String userId) {
        return this.sysUserMapper.selectUserRoleByUserId(userId);
    }

}
