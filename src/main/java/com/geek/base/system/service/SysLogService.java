package com.geek.base.system.service;

import com.geek.base.system.bean.SysLog;

import java.util.List;

public interface SysLogService {

    int insertLog(SysLog record);

    SysLog selectLogByLogId(String logId);

    List<SysLog> selectLogList(SysLog record);

}
