package com.geek.base.system.service;

import com.geek.base.system.bean.SysUser;
import com.geek.base.system.bean.SysUserRole;

import java.util.List;

public interface SysUserService {

    int insertUser(SysUser record);

    int updateUser(SysUser record);

    int deleteUser(String userId);

    int configUser(String userId, String roleId);

    List<SysUser> selectUserList(SysUser record);

    SysUser selectUserByUserId(String userId);

    SysUser selectUserByUserName(String userName);

    SysUserRole selectUserRoleByUserId(String userId);

}
