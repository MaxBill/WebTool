package com.geek.base.system.service;

import com.geek.base.common.bean.MenuBean;
import com.geek.base.common.bean.ZTreeBean;
import com.geek.base.system.bean.SysMenu;

import java.util.List;

public interface SysMenuService {

    int insertMenu(SysMenu record);

    int updateMenu(SysMenu record);

    int deleteMenu(String menuId);

    SysMenu selectMenuByMenuId(String menuId);

    List<SysMenu> selectMenuList(SysMenu record);

    List<SysMenu> selectParentMenuList();

    List<MenuBean> selectShowUserMenu(String userId);

    List<ZTreeBean> selectMenuTreeList();

    List<String> selectCheckMenuList(String roleId);

}
