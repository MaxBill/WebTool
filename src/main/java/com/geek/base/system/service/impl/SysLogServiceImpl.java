package com.geek.base.system.service.impl;

import com.geek.base.system.bean.SysLog;
import com.geek.base.system.dao.SysLogMapper;
import com.geek.base.system.service.SysLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @功能 系统日志业务实现
 * @作者 MaxBill
 * @时间 2018年7月30日
 * @邮箱 maxbill1993@163.com
 */
@Service
public class SysLogServiceImpl implements SysLogService {

    @Autowired
    private SysLogMapper sysLogMapper;

    public int insertLog(SysLog record) {
        return this.sysLogMapper.insert(record);
    }

    public SysLog selectLogByLogId(String logId) {
        return this.sysLogMapper.selectLogByLogId(logId);
    }

    public List<SysLog> selectLogList(SysLog record) {
        return this.sysLogMapper.selectLogList(record);
    }

}
