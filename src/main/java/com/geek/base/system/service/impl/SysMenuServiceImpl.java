package com.geek.base.system.service.impl;

import com.geek.base.common.bean.MenuBean;
import com.geek.base.common.bean.ZTreeBean;
import com.geek.base.system.bean.SysMenu;
import com.geek.base.system.dao.SysMenuMapper;
import com.geek.base.system.service.SysMenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @功能 系统菜单业务实现
 * @作者 MaxBill
 * @时间 2018年7月19日
 * @邮箱 maxbill1993@163.com
 */
@Service
public class SysMenuServiceImpl implements SysMenuService {

    @Autowired
    private SysMenuMapper sysMenuMapper;

    public int insertMenu(SysMenu record) {
        return this.sysMenuMapper.insert(record);
    }

    public int updateMenu(SysMenu record) {
        return this.sysMenuMapper.update(record);
    }

    public int deleteMenu(String menuId) {
        return this.sysMenuMapper.delete(menuId);
    }

    public SysMenu selectMenuByMenuId(String menuId) {
        return this.sysMenuMapper.selectMenuByMenuId(menuId);
    }

    public List<SysMenu> selectMenuList(SysMenu record) {
        return this.sysMenuMapper.selectMenuList(record);
    }

    public List<SysMenu> selectParentMenuList() {
        return this.sysMenuMapper.selectParentMenuList();
    }

    public List<MenuBean> selectShowUserMenu(String userId) {
        return this.sysMenuMapper.selectShowUserMenu(userId);
    }

    public List<ZTreeBean> selectMenuTreeList() {
        return this.sysMenuMapper.selectMenuTreeList();
    }

    public List<String> selectCheckMenuList(String roleId) {
        return this.sysMenuMapper.selectCheckMenuList(roleId);
    }


}
