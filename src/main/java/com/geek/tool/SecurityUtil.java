package com.geek.tool;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

public class SecurityUtil {


    //判断是否登录
    private static Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

    public static boolean isLogin() {
        if (authentication != null && !authentication.getPrincipal().equals("anonymousUser") && authentication.isAuthenticated()) {
            return true;
        } else {
            return false;
        }
    }

}
