package com.geek.tool;

import com.geek.base.business.bean.FtpInfo;
import com.geek.base.business.service.FtpInfoService;
import com.geek.base.system.bean.SysUser;
import com.jcraft.jsch.ChannelSftp;
import org.apache.commons.net.ftp.FTPClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class DataUtil {

    @Autowired
    private FtpInfoService ftpInfoService;

    public static SysUser getUser() {
        return (SysUser) HttpWebUtil.getSessionAttribute("sysUser");
    }

    public static String getUserId() {
        SysUser user = (SysUser) HttpWebUtil.getSessionAttribute("sysUser");
        return user == null ? null : user.getUserId();
    }

    public static String getUserName() {
        SysUser user = (SysUser) HttpWebUtil.getSessionAttribute("sysUser");
        return user == null ? null : user.getUserName();
    }

    public ChannelSftp getChannelSftp(String ftpId) throws Exception {
        Map sessionMap = (Map) HttpWebUtil.getSessionAttribute(ftpId);
        ChannelSftp sftp = (ChannelSftp) sessionMap.get("sftpObj");
        if (null == sftp || !sftp.isConnected()) {
            FtpInfo sftpBean = ftpInfoService.selectFtpInfoByFtpId(ftpId);
            sftp = SftpPoolUtil.openSftpConnect(sftpBean);
            HttpWebUtil.setSessionAttribute(ftpId, sftp);
        }
        return sftp;
    }

    public static FtpInfo getSftpInfo(String ftpId) {
        Map sessionMap = (Map) HttpWebUtil.getSessionAttribute(ftpId);
        FtpInfo ftpInfo = (FtpInfo) sessionMap.get("ftpInfo");
        return ftpInfo;
    }

    public FTPClient getFTPClient(String ftpId) {
        Map<String, Object> sessionMap = (Map<String, Object>) HttpWebUtil.getSessionAttribute(ftpId);
        FTPClient ftpClient = null;
        if (null == sessionMap || null == sessionMap.get("ftpObj")) {
            FtpInfo ftpInfo = ftpInfoService.selectFtpInfoByFtpId(ftpId);
            ftpClient = FtpPoolUtil.getFtpClient(ftpInfo);
            HttpWebUtil.setSessionAttribute(ftpId, ftpClient);
        } else {
            ftpClient = (FTPClient) sessionMap.get("ftpObj");
        }
        return ftpClient;
    }

}
