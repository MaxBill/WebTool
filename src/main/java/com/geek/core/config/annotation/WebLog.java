package com.geek.core.config.annotation;

import com.geek.base.common.enums.LogMsgs;
import com.geek.base.common.enums.LogType;

import java.lang.annotation.*;

@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface WebLog {

    LogMsgs msgs();

    LogType type();

}
