package com.geek.core.config.security;

import com.geek.base.system.bean.SysMenu;
import com.geek.base.system.bean.SysRole;
import com.geek.base.system.bean.SysUser;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;

public class SecurityUser extends SysUser implements UserDetails {

    //角色集合
    private SysRole role;

    public SecurityUser() {
        super();
    }

    public SecurityUser(SysUser user, SysRole role) {
        super(user);
        this.role = role;
    }

    //返回分配给用户的角色列表
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        if (null == role) {
            return AuthorityUtils.commaSeparatedStringToAuthorityList("");
        } else {
            if (null == role.getMenuList()) {
                return AuthorityUtils.commaSeparatedStringToAuthorityList("");
            }
            StringBuilder commaBuilder = new StringBuilder();
            for (SysMenu menu : role.getMenuList()) {
                commaBuilder.append(menu.getMenuCode());
                commaBuilder.append(",");
            }
            String authorities = commaBuilder.substring(0, commaBuilder.length() - 1);
            return AuthorityUtils.commaSeparatedStringToAuthorityList(authorities);
        }
    }

    public String getId() {
        return super.getUserId();
    }

    public String getPassword() {
        return super.getUserPass();
    }

    public String getUsername() {
        return super.getUserName();
    }

    // 账户是否未过期
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    // 账户是否未锁定
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    // 密码是否未过期
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    // 账户是否激活
    @Override
    public boolean isEnabled() {
        return true;
    }

}
