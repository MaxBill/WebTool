package com.geek.core.config.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.ArrayList;
import java.util.List;

@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {


    @Autowired
    private SysUserDetailsHandler userDetailsHandler;

    @Autowired
    private SysUserDetailsService userDetailsService;


    /**
     * 访问权限配置
     */
    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {
        //关闭csrf防护
        httpSecurity.csrf().disable();
        //禁用缓存
        httpSecurity.headers().cacheControl();
        //允许frame
        httpSecurity.headers().frameOptions().sameOrigin();
        //允许所有用户访问的文件
        //httpSecurity.authorizeRequests().antMatchers(HttpMethod.GET).permitAll();
        // 允许对于网站静态资源的无授权访问
        httpSecurity.authorizeRequests().antMatchers("/image/**", "/style/**", "/plugin/**", "/script/**").permitAll();
        httpSecurity.authorizeRequests().antMatchers("/auth").permitAll();
        //其他地址访问均需验证权限
        httpSecurity.authorizeRequests().anyRequest().authenticated();
        //指定登录地址是/login
        httpSecurity.formLogin().loginPage("/auth");
        httpSecurity.formLogin().loginProcessingUrl("/login").permitAll();
        //登录成功后默认跳转地址是/root
        //httpSecurity.formLogin().defaultSuccessUrl("/root", true).permitAll();
        //登录成功后跳转自定义处理器
        httpSecurity.formLogin().successHandler(userDetailsHandler);
        //登录失败后跳转自定义处理器
        httpSecurity.formLogin().failureHandler(userDetailsHandler);
        //退出请求处理器
        //退出请求地址
        httpSecurity.logout().logoutUrl("/logout");
        //退出后跳转地址是/login
        httpSecurity.logout().logoutSuccessUrl("/auth");
        //退出后清除会话信息
        httpSecurity.logout().invalidateHttpSession(true);
        //退出后清除认证信息
        httpSecurity.logout().clearAuthentication(true);
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        //auth.userDetailsService(userDetailsService);
        auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    public static void main(String[] args) {
        System.out.println(new SecurityConfig().passwordEncoder().encode("123456"));
    }

}
