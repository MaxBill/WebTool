package com.geek.core.config.security;

import com.geek.base.system.bean.SysRole;
import com.geek.base.system.bean.SysUser;
import com.geek.base.system.service.SysRoleService;
import com.geek.base.system.service.SysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class SysUserDetailsService implements UserDetailsService {

    @Autowired
    private SysUserService sysUserService;

    @Autowired
    private SysRoleService sysRoleService;

    @Override
    public UserDetails loadUserByUsername(String userName) {
        SysUser user = sysUserService.selectUserByUserName(userName);
        if (user == null) {
            throw new UsernameNotFoundException("Not Found User");
        } else {
            try {
                SysRole role = sysRoleService.selectRoleByUserId(user.getUserId());
                return new SecurityUser(user, role);
            } catch (Exception e) {
                throw new UsernameNotFoundException("Not Found UserRole");
            }
        }
    }


}
