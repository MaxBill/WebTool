package com.geek.core.aspect;

import com.alibaba.fastjson.JSON;
import com.geek.base.system.bean.SysLog;
import com.geek.base.system.bean.SysUser;
import com.geek.base.system.service.SysLogService;
import com.geek.core.config.annotation.WebLog;
import com.geek.tool.BrowseUtil;
import com.geek.tool.DataUtil;
import com.geek.tool.HttpWebUtil;
import com.geek.tool.KeyGeneratorUtil;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Date;

@Slf4j
@Aspect
@Component
public class SysLogAspect {

    @Autowired
    private SysLogService sysLogService;

    private SysLog sysLog;

    @Pointcut("@annotation(com.geek.core.config.annotation.WebLog)")
    public void runAspect() {
    }

    //@Pointcut("execution(public * com.geek.base..*.*(..))")
    //public void runAspect() {}

    @Before("runAspect()")
    public void doBefore(JoinPoint joinPoint) {
        sysLog = new SysLog();
        // 接收到请求，记录请求内容
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        Method method = signature.getMethod();
        WebLog webLog = method.getAnnotation(WebLog.class);
        // 记录下请求内容
        log.info("<<<<<<<<<<<<<<<<<<<<<<<<");
        log.info("URL : " + request.getRequestURL().toString());
        log.info("HTTP_METHOD : " + request.getMethod());
        log.info("IP : " + request.getRemoteAddr());
        log.info("CLASS_METHOD : " + joinPoint.getSignature().getDeclaringTypeName() + "." + joinPoint.getSignature().getName());
        log.info("ARGS : " + Arrays.toString(joinPoint.getArgs()));
        log.info("UA : " + request.getHeader("User-Agent"));
        //持久化数据准备
        sysLog.setLogId(KeyGeneratorUtil.getUUIDKey());
        sysLog.setLogUser(DataUtil.getUserName());
        sysLog.setLogAddr(request.getRemoteAddr());
        sysLog.setLogUrls(request.getRequestURL().toString());
        sysLog.setLogPath(joinPoint.getSignature().getDeclaringTypeName() + "." + joinPoint.getSignature().getName());
        sysLog.setLogMethod(request.getMethod());
        if (null != webLog) {
            sysLog.setLogName(webLog.msgs().getValue());
            sysLog.setLogType(webLog.type().getValue());
        }
        sysLog.setLogBrowser(BrowseUtil.checkBrowseType(request.getHeader("User-Agent")));
        sysLog.setLogSystem(BrowseUtil.checkSystemType(request.getHeader("User-Agent")));
        sysLog.setLogReqParam(JSON.toJSONString(Arrays.toString(joinPoint.getArgs())));
    }

    @AfterReturning(returning = "res", pointcut = "runAspect()")
    public void doAfterReturning(Object res) {
        // 处理完请求，返回内容
        log.info("RESPONSE : " + res);
        log.info(">>>>>>>>>>>>>>>>>>>>>>>>>");
        if (null != res) {
            sysLog.setLogResParam(JSON.toJSONString(res));
        }
        sysLog.setLogTime(new Date());
        //日志写入数据库
        this.sysLogService.insertLog(sysLog);
        sysLog = null;
    }
}
