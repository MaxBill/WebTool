package com.geek;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.MultipartConfigFactory;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.annotation.Bean;

import javax.servlet.MultipartConfigElement;

import static javafx.application.Application.launch;

@ServletComponentScan
@SpringBootApplication
@MapperScan("com.geek.base.*.dao")
public class RunApplication {

    @Bean
    public MultipartConfigElement multipartConfigElement() {
        MultipartConfigFactory factory = new MultipartConfigFactory();
        //设置文件大小限制
        factory.setMaxFileSize("1024MB"); //KB,MB
        //设置总上传数据总大小
        factory.setMaxRequestSize("10240MB");
        //factory.setLocation("路径地址");
        return factory.createMultipartConfig();
    }

    public static void main(String[] args) {
        SpringApplication.run(RunApplication.class, args);
    }


}
