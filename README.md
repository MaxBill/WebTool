# WebToolPlus

#### 项目介绍
基于SpringBoot2.0的超强的Web在线运维管理工具，包含在线SFTP、FTP；在线SSH管理工具；在线数据库管理工具；在线对象存储管理工具等

#### 特色功能

1.sftp管理（100%）<br/> 
2.ftp 管理（80%）<br/> 
3.ftps管理（开发中）<br/> 
4.网盘管理（未开始）<br/> 
5.数据库管理（未开始）<br/> 
6.对象存储管理（未开始）<br/> 
7.SSH远程终端（未开始）<br/> 
8.基础用户角色权限模块（已完成）<br/> 

#### 软件架构


1.后端技术架构 <br/> 
  SpringBoot2.0<br/> 
  Mybatis<br/> 
  Druid<br/> 
  Redis<br/> 
  Pagehelper<br/> 
  Jsch<br/> 
  ...<br/> 
2.前端技术架构<br/> 
  jquery<br/> 
  echarts<br/> 
  layui<br/> 
  ...<br/> 
  

#### 安装使用说明

1. 检出项目
2. 创建数据库执行项目带的脚本
3. 打包运行
4. 浏览器中访问http://ip:8080即可

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request

#### 项目截图

1.FTP操作截图

![输入图片说明](https://images.gitee.com/uploads/images/2018/0820/101615_3873280a_1252126.png "深度截图_Desktop_20180820095532.png")

![输入图片说明](https://images.gitee.com/uploads/images/2018/0820/101700_4d34d43c_1252126.png "深度截图_Desktop_20180820095553.png")

2.基础模块截图

![输入图片说明](https://images.gitee.com/uploads/images/2018/0820/101757_1bcb95a2_1252126.png "深度截图_Desktop_20180820093943.png")

![输入图片说明](https://images.gitee.com/uploads/images/2018/0820/101809_8b739906_1252126.png "深度截图_Desktop_20180820094109.png")

![输入图片说明](https://images.gitee.com/uploads/images/2018/0820/101819_101032b5_1252126.png "深度截图_Desktop_20180820094223.png")

![输入图片说明](https://images.gitee.com/uploads/images/2018/0820/101836_c8bb059a_1252126.png "深度截图_Desktop_20180820095333.png")

![输入图片说明](https://images.gitee.com/uploads/images/2018/0820/101854_56059fe0_1252126.png "深度截图_Desktop_20180820095434.png")

![输入图片说明](https://images.gitee.com/uploads/images/2018/0820/101908_542eeb99_1252126.png "深度截图_Desktop_20180820095448.png")